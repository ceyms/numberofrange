var numberOne = getElementById("numberOne");
var numberTwo = getElementById("numberTwo");
var showNumberButton1 = getElementById("showNumberButton1");
var showNumberButton2 = getElementById("showNumberButton2");

function getElementById(id) {
    return document.getElementById(id);
}

function showMessage(text) {
    var showNumber = getElementById("showNumber");
    showNumber.innerHTML = text;
}

function showNumberMethod() {
    var showNumber = getElementById("showNumber");
    showNumber.innerHTML = "";
    var numberOneValue = numberOne.value;
    var numberTwoValue = numberTwo.value;
    var counterFirst = parseInt(numberOneValue) + 1;
    var allNumbers = "";
    if (numberOneValue != "" && numberTwoValue != "") {
        for (var counter = counterFirst; counter < numberTwoValue; counter++) {
            allNumbers += counter + " ";
            showMessage("Başarılı");
        }
        showNumber.innerHTML += numberOneValue + " ile " + numberTwoValue + " arasındaki rakamlar: " + allNumbers;
    } else {
        showMessage("Lütfen bir rakam giriniz");
    }
};

showNumberButton1.addEventListener('click', showNumberMethod);
showNumberButton2.addEventListener('click', showNumberMethod);

/* // just click function
showNumberButton.addEventListener('click', function() {
    var numberOneValue = numberOne.value;
    var numberTwoValue = numberTwo.value;
    var counterFirst = parseInt(numberOneValue) + 1;
    var allNumbers = "";
    if (numberOneValue != "" && numberTwoValue != "") {
        for (var counter = counterFirst; counter < numberTwoValue; counter++) {
            allNumbers += counter + " ";
        }
        var showNumber = document.getElementById("showNumber");
        showNumber.innerHTML += numberOneValue + " ile " + numberTwoValue + " arasındaki rakamlar: " + allNumbers;
    } else {
        var showNumber = document.getElementById("showNumber");
        showNumber.innerHTML = "Lütfen Bir Rakam Giriniz";
    }
});
*/



